# snippets

Useful code and configuration snippets

* [Bash snippets](./bash/README.md)
* [CSS snippets](./css/README.md)
