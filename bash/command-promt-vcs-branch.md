# Show current git/mercurial branch in command prompt

Put this snippet into `.bashrc` file inside your profile folder.

```bash
search_up() {
    local look=${PWD%/}

    while [[ -n $look ]]; do
        [[ -e $look/$1 ]] && {
            printf '%s\n' "$look"
            return
        }

        look=${look%/*}
    done

    [[ -e /$1 ]] && echo /
}
parse_vcs_branch() {
    if search_up '.git' > /dev/null ; then
        echo " (`git branch | grep '*' | cut -d ' ' -f2`)"
    elif search_up '.hg' > /dev/null ; then
        echo " (`hg branch`)"
    fi
}
export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_vcs_branch)\[\033[00m\] $ "
```
